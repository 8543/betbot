(function () {

    /*
     * Requires use with an EMCAScript6 client
     * https://kangax.github.io/compat-table/es6/
     */

    'use strict';
    sounds_rolling.volume = 0.0; // fuck you

    var logging = {
        log: function (str) {
            console.log(str);
        },
        error: function (str) {
            console.error(str);
        }
    };

    class BettingStrategy {
        // int last_bet
        // string name

        constructor() {
        }

        static place_bet(won, bet, balance) {
            logging.error("Call to pure virtual function");
        }
    }

    class Martingale extends BettingStrategy {

        // base quant

        constructor() {
            super();
            this.base_ratio = Martingale.to_rat(2);
            this.cur_ratio = this.base_ratio;
        }

        static place_bet(won, bet, balance) {
            if (won) {
                this.cur_ratio = this.base_ratio;
            } else {
                this.cur_ratio = this.cur_ratio * 2;
            }

            return this.cur_ratio * balance;
        }

        static to_rat(f_num) {
            return (f_num / 100);
        }
    }

    class Net {
        // WebSocket socket
        // string entry_token;
        // Bot par

        static send(data) {
            if (typeof data != "string") {
                data = JSON.stringify(data);
            }
            if (this.socket && this.socket.readyState == 1) {
                this.socket.send(data);
            }
        }

        static getToken() {
            $.ajax({
                url: "/scripts/getToken.php",
                success: function (data) {
                    return data;
                }
            });
        }

        onReceive(data) {
            var rec = JSON.parse(data.data);

            switch (rec.type) {

                case "roll":
                    // Outcome of previous bet
                    var won = (rec.roll <= 7);
                    var bet = this.par.requestBet(won);

                    break;
                case "hello":
                    logging.log("Balance: "+rec.balance);
                    this.par.balance = rec.balance;

                    break;

            }
        }

        constructor(parent, headless) {

            this.par = parent;

            if (headless) {
                this.socket = new WebSocket("ws://www.csgodouble.com:8080/" + entry_token);
                // Get token
                this.entry_token = this.getToken();
                switch (this.entry_token) {

                    case "nologin":
                        logging.error("Must login through steam first on the index page of CSGODouble");
                        break;
                    case "max":
                        logging.error("CSGODouble servers are full");
                        break;
                }

            } else {
                this.entry_token = "";
                this.socket = WS; // CSGODOuble is nice enough to make their socket global.
                // Get (already init'd) balance
                this.par.balance = Math.floor(str2int($("#balance").html()));
            }

            // Hooks
            this.socket.onerror = this.socket.onclose = function (event) {
                logging.error("Socket was closed");
            };

            // this changes from scope
            (function (that) {
                that.socket.onmessage = function (data) {
                    // Connect to CSGODouble's built function if not headless
                    if (!headless) {
                        onMessage(data);
                    }
                    that.onReceive(data);
                }
            })(this);

        }
    }

    // template <T>
    class Bot {
        // bool headless_mode;
        // Net networking

        // int balance
        // T strategy


        constructor(T) {
            bootbox.confirm("Would you like to run the bot in headless mode? Headless mode disables sounds, animations, chat etc.", function (result) {

                // cont..
                this.headless_mode = result;
                this.networking = new Net(this, this.headless_mode);
                this.strategy = new T();
            });
        }

        set balance(rhs) {
            this.strategy.balance = rhs;
        }

        requestBet(won) {
            return this.strategy.place_bet(won);
        }
    }

    // init
    var bot = new Bot(Martingale);


})();